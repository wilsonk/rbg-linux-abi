extern crate walkdir;
extern crate regex;
extern crate itertools;
extern crate git2;
extern crate clap;
#[macro_use]
extern crate lazy_static;
extern crate clang;

use walkdir::{WalkDirIterator, DirEntry, WalkDir};
use regex::RegexBuilder;
use itertools::Itertools;
use clap::{Arg, App};
use git2::Repository;
use clang::{Clang, Index};

use std::collections::HashMap;
use std::collections::hash_map::Entry::{Occupied, Vacant};
use std::fs::File;
use std::fs;
use std::path::{Path, PathBuf};
use std::io::prelude::*;

lazy_static! {
    static ref C: Clang = Clang::new().unwrap();
}

fn should_visit(entry: &DirEntry) -> bool {
    !entry.file_name()
          .to_str()
          .map(|s| {
              s.starts_with(".") || s.contains("kernel-doc") || s == "compat.h" || s == "syscalls.h"
          })
          .unwrap_or(false)
}

fn extract(dir: &Path) -> Vec<(String, String)> {
    let mut res = Vec::new();

    let sysdefine = match RegexBuilder::new(r"(COMPAT_)?SYSCALL_DEFINE\d.*?\)")
                              .dot_matches_new_line(true)
                              .compile() {
        Ok(r) => r,
        Err(e) => panic!("err: {}", e),
    };

    let archextr = RegexBuilder::new(r"arch/([:alnum:]+)/").compile().unwrap();

    let mut buf = Vec::with_capacity(1024 * 1024);
    for p in WalkDir::new(dir)
                 .into_iter()
                 .filter_entry(should_visit)
                 .filter_map(|e| e.ok()) {
        buf.truncate(0);
        if !p.file_type().is_file() {
            continue;
        }
        let mut f = File::open(p.path()).unwrap();
        f.read_to_end(&mut buf).unwrap();
        let s = match std::str::from_utf8(&buf) {
            Ok(s) => s,
            Err(_) => continue,
        };

        let fp = format!("{}", p.path().display());

        let arch: String = match archextr.captures(&fp) {
            Some(c) => c.at(1).unwrap().into(),
            None => "generic".into(),
        };

        for c in sysdefine.captures_iter(s) {
            let (beg, end) = c.pos(0).unwrap();
            res.push((arch.clone(), s[beg..end].replace("\n", " ")));
        }
    }
    res
}

#[derive(Debug, PartialEq, Clone)]
enum Type {
    Scalar(String),
    Struct(String),
    Enum(String),
    Union(String),
    Const(Box<Type>),
    User(Box<Type>),
    PointerTo(Box<Type>),
    Void,
}

#[derive(Debug, PartialEq, Clone)]
struct Syscall {
    name: String,
    compat: bool,
    args: Vec<(String, Type)>,
}

fn parse_type(s: &str) -> Type {
    if s == "void" {
        Type::Void
    } else if !s.contains(" ") {
        Type::Scalar(s.into())
    } else if s == "unsigned long" {
        Type::Scalar(s.into())
    } else if s == "unsigned int" {
        Type::Scalar(s.into())
    } else if s == "unsigned char" {
        Type::Scalar(s.into())
    } else if s.ends_with("*") {
        Type::PointerTo(Box::new(parse_type(s[..s.len() - 1].trim())))
    } else if s.ends_with(" __user") {
        Type::User(Box::new(parse_type(&s[..s.len() - 7].trim())))
    } else if s.starts_with("const ") {
        parse_type(&s[6..].trim())
    } else if s.ends_with("const") {
        Type::Const(Box::new(parse_type(&s[..s.len() - 5].trim())))
    } else if s.starts_with("enum ") {
        Type::Enum(s[5..].into())
    } else if s.starts_with("struct ") {
        Type::Struct(s[7..].into())
    } else if s.starts_with("union ") {
        Type::Union(s[5..].into())
    } else {
        panic!("Unknown syscall argument type: {}", s)
    }
}

impl Type {
    fn rust(&self) -> String {
        use Type::*;
        match self {
            &Scalar(ref s) => {
                match &**s {
                    "char" => "i8",
                    "short" => "i16",
                    "int" => "i32",
                    "long" => "isize",
                    "long long" => "i64",
                    "unsigned" => "u32",
                    "unsigned char" => "u8",
                    "unsigned short" => "u16",
                    "unsigned int" => "u32",
                    "unsigned long" => "usize",
                    "u64" => "u64",
                    "u32" => "u32",
                    "u16" => "u16",
                    "u8" => "u8",
                    "s64" => "i64",
                    "s32" => "i32",
                    "s16" => "i16",
                    "s8" => "i8",
                    "__u64" => "u64",
                    "__u32" => "u32",
                    "__u16" => "u16",
                    "__u8" => "u8",
                    "__s64" => "i64",
                    "__s32" => "i32",
                    "__s16" => "i16",
                    "__s8" => "i8",
                    "uint64_t" => "u64",
                    "uint32_t" => "u32",
                    "uint16_t" => "u16",
                    "uint8_t" => "u8",
                    "fd_set" => "FDSet",
                    "pid_t" => "i32",
                    "size_t" => "usize",
                    "ssize_t" => "isize",
                    "off_t" => "isize",
                    "loff_t" => "i64",
                    "sigset_t" => "SigSet",
                    "umode_t" => "u16",
                    "uint" => "u32",
                    "compat_ulong_t" => "u32",
                    "compat_uptr_t" => "u32",
                    "compat_size_t" => "u32",
                    "compat_ssize_t" => "i32",
                    "utrap_entry_t" => "i32",
                    "utrap_handler_t" => "*mut u8",
                    "aio_context_t" => "usize",
                    "compat_aio_context_t" => "u32",
                    "compat_long_t" => "i32",
                    "compat_sigset_t" => "CompatSigSet",
                    "compat_off_t" => "i32",
                    "uid_t" => "u16",
                    "gid_t" => "u16",
                    "qid_t" => "u32",
                    "uid32_t" => "u32",
                    "compat_loff_t" => "i64",
                    "compat_mode_t" => "CompatMode",
                    "mqd_t" => "i32",
                    "key_t" => "i32",
                    "compat_key_t" => "i32",
                    "cap_user_header_t" => "UserPtr<CapUserHeader>",
                    "cap_user_data_t" => "UserPtr<CapUserData>",
                    "compat_old_sigset_t" => "u32",
                    "compat_pid_t" => "i32",
                    "compat_uint_t" => "u32",
                    "clockid_t" => "i32",
                    "timer_t" => "i32",
                    "compat_timer_t" => "i32",
                    "time_t" => "isize",
                    "compat_time_t" => "i32",
                    "siginfo_t" => "SigInfo",
                    "compat_siginfo_t" => "CompatSigInfo",
                    "stack_t" => "Stack",
                    "compat_stack_t" => "CompatStack",
                    "old_sigset_t" => "isize",
                    "__sighandler_t" => "UserPtr<u8>",
                    "old_uid_t" => "u16",
                    "old_gid_t" => "u16",
                    "key_serial_t" => "i32",
                    _ => panic!("unknown scalar type `{}`", s),
                }
                .into()
            }
            &Struct(ref s) => format!("struct_{}", s),
            &Enum(ref s) => format!("enum_{}", s),
            &Union(ref s) => format!("union_{}", s),
            &Const(ref t) => format!("Const<{}>", t.rust()),
            &User(ref t) => format!("User<{}>", t.rust()),
            &PointerTo(ref t) => {
                if let &User(ref u) = &**t {
                    format!("UserPtr<{}>", u.rust())
                } else {
                    format!("*mut {}", t.rust())
                }
            }
            &Void => "void".into(),
        }
    }
}

fn parse_syscall(mut s: &str) -> Syscall {
    let compat = if s.starts_with("COMPAT_") {
        s = &s[7..]; // skip COMPAT_
        true
    } else {
        false
    };
    s = &s[14..]; // skip SYSCALL_DEFINE
    let ct: usize = s[..s.find('(').unwrap()].parse().unwrap();
    s = &s[s.find('(').unwrap() + 1..]; // skip initial %d(
    s = &s[..s.len() - 1]; // skip trailing )

    let mut split = s.split(",").fuse(); // non-judiciously calling next twice

    let name = split.next().expect("no name for syscall!").into();

    let mut args = Vec::new();


    while let (Some(ty), Some(nm)) = (split.next(), split.next()) {
        let nm = nm.trim();
        let ty = ty.trim();
        args.push((nm.into(), parse_type(ty)));
    }

    assert!(ct == args.len(),
            "malformed syscall: number of args != name declared {}",
            s);

    Syscall {
        name: if compat {
            format!("compat_{}", name)
        } else {
            name
        },
        compat: compat,
        args: args,
    }
}

fn extract_scno_defs(arch_dir: PathBuf) -> Vec<Vec<String>> {
    use clang::{EntityKind, EntityVisitResult};
    use clang::diagnostic::Severity::{Error, Fatal};

    let idx = Index::new(&C, false, false);
    let tu = idx.parser(arch_dir.join("include/uapi/asm/unistd.h"))
                .detailed_preprocessing_record(true)
                .incomplete(true)
                .skip_function_bodies(true)
                .arguments(&vec!["-E",
                                 "-x",
                                 "c",
                                 "-I",
                                 &*format!("{}", arch_dir.join("include").display()),
                                 "-I",
                                 &*format!("{}", arch_dir.join("../../include").display()),
                                 "-I",
                                 &*format!("{}", arch_dir.join("include/uapi").display())])
                .parse()
                .unwrap();
    let mut failed = false;
    for diag in tu.get_diagnostics() {
        if diag.get_severity() == Error || diag.get_severity() == Fatal {
            println!("{}", diag);
            failed = true;
        }
    }
    if failed {
        return Vec::new();
    }

    let mut nr_defs = Vec::new();

    let root_ent = tu.get_entity();
    root_ent.visit_children(|cur, _| {
        if cur.get_kind() != EntityKind::MacroDefinition {
            return EntityVisitResult::Recurse;
        }
        let nm = cur.get_name().unwrap();
        if nm.starts_with("__NR") {
            nr_defs.push(cur.get_range()
                            .unwrap()
                            .tokenize()
                            .iter()
                            .map(|t| t.get_spelling())
                            .collect())
        }
        EntityVisitResult::Recurse
    });

    nr_defs
}

fn parse_scnos(mut to_process: Vec<Vec<String>>) -> HashMap<String, String> {
    let mut scnos = HashMap::new();
    for macr in to_process.drain(..) {
        let name = macr[0].clone();
        if scnos.contains_key(&name) {
            if name != "__NR_syscalls" {
                println!("doubly-defined scno! {:?} old {:?}", macr, scnos.get(&name));
                continue;
            }
        }
        match macr[1].parse::<i64>() {
            Ok(_) => {
                scnos.insert(name.clone(), macr[1].clone());
            }
            Err(_) => {
                if macr[1].starts_with("0x") {
                    scnos.insert(name.clone(), macr[1].clone());
                    continue;
                }
                if scnos.contains_key(&macr[1]) {
                    let other = scnos.get(&macr[1]).unwrap().clone();
                    scnos.insert(name.clone(), other);
                } else if macr[1] == "-" {
                    scnos.insert(name.clone(),
                                 format!("-{}", macr[2].parse::<i64>().unwrap()));
                } else if macr[1] == "(" && macr.len() == 5 {
                    let other = scnos.get(&macr[2]).unwrap().clone();
                    scnos.insert(name.clone(), other);
                } else if macr[1] == "(" && macr.len() >= 6 {
                    let base = &macr[2];
                    let op = &macr[3];
                    let offset = &macr[4];
                    let close = &macr[5];
                    if close != ")" || op != "+" {
                        println!("malformed expr! {:?}", macr);
                        continue;
                    }
                    if !scnos.contains_key(base) {
                        println!("use-before-defined: {:?}", macr);
                        continue;
                    }
                    match offset.parse::<i64>() {
                        Ok(_) => {
                            scnos.insert(name.clone(), format!("{} + {}", base, offset.clone()));
                        }
                        Err(_) => {
                            println!("malformed expr! {:?}", macr);
                        }
                    }
                    if scnos.contains_key(base) {

                    }
                } else {
                    panic!("unhandled __NR def {:?}", macr);
                }
            }
        }
    }

    scnos
}

fn make_dispatch(arch_dir: PathBuf, syscalls: &HashMap<String, Syscall>) -> Vec<u8> {
    let arch_name: String = arch_dir.clone().file_name().unwrap().to_str().unwrap().into();
    let scnos = parse_scnos(extract_scno_defs(arch_dir));
    let mut to_dispatch = Vec::new();
    let mut unknown = Vec::new();
    for key in scnos.keys() {
        let name = key.replace("__NR_", "");
        let generic = format!("sys_generic_{}", name);
        let arch = format!("sys_{}_{}", arch_name, name);
        if syscalls.contains_key(&arch) {
            to_dispatch.push((arch, scnos.get(key).unwrap().clone()));
        } else if syscalls.contains_key(&generic) {
            to_dispatch.push((generic, scnos.get(key).unwrap().clone()));
        } else {
            if name.contains("available") || name.contains("reserved") ||
               name == "syscall_count" || name == "syscalls" {
                continue;
            } else if name.starts_with("__NR3264_") {
                // FIXME: figure out what these even mean.
                continue;
            }
            unknown.push(name);
        }
    }

    let mut f = Vec::new();

    to_dispatch.sort_by_key(|k| k.1.clone().parse::<i64>().unwrap_or(0));

    writeln!(&mut f,
             "pub fn dispatch_{}<T: ::UserContext>(user: T) -> Option<isize> {{",
             arch_name)
        .unwrap();
    writeln!(&mut f, "    match user.scno() {{").unwrap();
    for (name, scno) in to_dispatch {
        writeln!(&mut f,
                 "        {} => Some(::funcs::{}({})),",
                 scno,
                 name,
                 (0..syscalls.get(&name).unwrap().args.len())
                     .map(|i| format!("user.get_arg{}()", i))
                     .join(", "))
            .unwrap();
    }
    writeln!(&mut f, "        _ => None,").unwrap();
    writeln!(&mut f, "    }}").unwrap();
    writeln!(&mut f, "}}").unwrap();

    f
}

fn main() {
    let matches = App::new("Linux system call translator generator")
                      .version("0.1.0")
                      .author("Corey Richardson <corey@octayn.net>")
                      .about("Creates templates and boilerplate for implementing Linux \
                      ABI-compatible system call translators. In particular, it handles and
                      abstracts away some of the details of system call decoding and
                      dispatching.")
                      .arg(Arg::with_name("repo")
                               .short("r")
                               .long("kernel-repo")
                               .value_name("PATH")
                               .help("Path to a Linux kernel git checkout")
                               .required(true)
                               .takes_value(true))
                      .arg(Arg::with_name("outdir")
                               .short("o")
                               .long("out-dir")
                               .value_name("PATH")
                               .help("Directory to leave template files")
                               .required(true)
                               .takes_value(true))
                      .get_matches();

    let mut hm: HashMap<String, Syscall> = HashMap::new();

    let pb = PathBuf::from(matches.value_of("repo").unwrap());
    let od = PathBuf::from(matches.value_of("outdir").unwrap());

    let repo = Repository::open(&pb).unwrap();
    let head = repo.head().unwrap().resolve().unwrap();
    let kv = std::str::from_utf8(head.name_bytes()).unwrap();

    let mut funcs = Vec::<u8>::new();
    writeln!(&mut funcs, "#![allow(unused_variables)];\n").unwrap();
    writeln!(&mut funcs, "use types::*;\n").unwrap();
    writeln!(&mut funcs, "use consts::*;\n").unwrap();

    for (arch, sys) in extract(&pb).into_iter().map(|(arch, x)| (arch, parse_syscall(&x))) {
        // manually bind these; it needs some mangling based on arch.
        if sys.name == "clone" || sys.name == "sigsuspend" {
            continue;
        }
        let name = if !sys.name.contains(&*arch) {
            format!("sys_{}_{}", arch, sys.name)
        } else {
            format!("sys_{}", sys.name)
        };
        match hm.entry(name.clone()) {
            Occupied(o) => {
                // assert!(arch == "generic", "wut {} {:?} old {:?}", name, sys, o.get());
                assert!(o.get()
                         .args
                         .iter()
                         .zip(sys.args.iter())
                         .all(|(&(_, ref a2), &(_, ref b2))| a2 == b2),
                        "unmatched args in {} {:?} vs {:?}",
                        name,
                        o.get(),
                        sys);
            }
            Vacant(v) => {
                v.insert(sys.clone());
                writeln!(&mut funcs,
                         "pub fn {}({}) -> isize {{\n    -ENOSYS\n}}",
                         name,
                         sys.args
                            .iter()
                            .map(|&(ref n, ref t)| format!("{}: {}", n, t.rust()))
                            .join(", "))
                    .unwrap();
            }
        }
    }

    File::create(od.join("funcs.rs")).unwrap().write_all(&funcs).unwrap();

    let mut crate_root = Vec::new();

    writeln!(&mut crate_root,
             "/* Generated by scrape-tool from kernel revision {} */",
             kv)
        .unwrap();
    writeln!(&mut crate_root, "pub mod types;").unwrap();
    writeln!(&mut crate_root, "pub mod funcs;").unwrap();

    for arch_dir in fs::read_dir(pb.join("arch"))
                        .unwrap()
                        .filter(|e| e.as_ref().unwrap().file_type().unwrap().is_dir()) {
        let p = arch_dir.unwrap().path();
        let unistd = p.join("include/uapi/asm/unistd.h");
        if unistd.exists() {
            let mut f = Vec::new();
            writeln!(&mut funcs,
                     "/* Generated by scrape-tool from kernel revision {} */",
                     kv)
                .unwrap();
            f.extend(make_dispatch(p.clone(), &hm));
            let arch_name = p.file_name().unwrap().to_str().unwrap();
            File::create(od.join(format!("{}_dispatch.rs", arch_name)))
                .unwrap()
                .write_all(&f)
                .unwrap();
            writeln!(&mut crate_root, "pub mod {}_dispatch;", arch_name).unwrap();
        }
    }

    File::create(od.join("lib.rs")).unwrap().write_all(&crate_root).unwrap();

}
